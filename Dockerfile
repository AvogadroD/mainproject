# syntax=docker/dockerfile:1

# ARG PYTHON_VERSION=3.11.0
# FROM python:${PYTHON_VERSION}-slim
FROM python:3.9

WORKDIR /app

RUN pip install --no-cache-dir redis
RUN pip install --no-cache-dir uvicorn
RUN pip install --no-cache-dir fastapi
RUN pip install --no-cache-dir python-multipart

COPY . .

# Expose the port that the application listens on.
# EXPOSE 8000

# Run the application. uvicorn main:app --host 0.0.0.0 --port 8080 --reload
CMD [ "uvicorn", "main:app", "--host", "0.0.0.0", "--port", "80" ]