from fastapi import FastAPI, Form
from redis import Redis

import random
import sys
import json

def search_binary(numbers : list, search_for : int, low : int = None, high : int = None):
    if low is None:
        low = 0
    if high is None:
        high = len(numbers) - 1
    if high >= low:
        mid = (high + low) // 2

        if numbers[mid] == search_for:
            return mid
        
        elif numbers[mid] > search_for:
            return search_binary(numbers, search_for, low, mid - 1)
        
        else:
            return search_binary(numbers, search_for, mid + 1, high)
        
    else:
        return -1


app = FastAPI()
r = Redis(host='redis', port=6379, db=0)
if r.exists('numbers'):
    numbers = json.loads(r.get('numbers'))
else:
    numbers = []
    for i in range(100):
        numbers.append(random.randint(1, 100))
    numbers = sorted(numbers)
    r.set("numbers", json.dumps(numbers))

@app.post("/number_where")
def find_number(number: int = Form(...)):
    index = search_binary(numbers, number)
    return index
